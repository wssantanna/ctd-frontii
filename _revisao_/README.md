# Aula Bonus - Revisão

Revisão dos fundamentos do Javascript aplicado em um projeto fictício.

## Tópicos abordados

- Variáveis;

- Tipo de variáveis;

- Funções;

- Requisições assíncronas;

- Boas práticas;

## Gravação da aula

- [Link da Gravacão](https://drive.google.com/file/d/1pC7F9Jt6levRfR-PhwyAryCUiG9wwbs2/view)